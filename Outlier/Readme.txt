Brief Design Description
----------------------------------------------------------------------------------------------------

The Outlier solution contains the following projects.

- Outlier                   It contains the main command line program. It loads the DLLs for reading
                            data points, identifying outliers, and writing data points and passes
                            the objects created by these DLLs to DataPointProcessor to do the work.
                            It is designed to depend only on the abstract interfaces in OutlierModel
                            so that it is free of dependencies of actual implementations of these
                            abstract interfaces. In addition, it is designed to allow change of
                            abstract interface implementations via app.config changes at runtime.

- OutlierModel              It contains the abstract interfaces for reading data points
                            (IDataSource), identifying outliers (IOutlierDetector), and writing data
                            points (IDataWriter).

- CSVDataSource             An implementation of IDataSource that reads data points from CSV file.

- CSVDataWriter             An implementation of IDataWriter that writes data points to CSV file.

- SimpleOutlierDetector     A simple implementation of IOutlierDetector. If a data point is 3 sigma
                            away (configurable) from the accumulated average, then it is considered
                            an outlier. The first 10 data points (configurable) are excluded from
                            outlier detection.

- StdinDataSource           Another implementation of IDataSource that reads data points form
                            console standard input. It is meant to illustrate that the Outlier
                            program can be changed to use different data source via configuration.

- StdoutDataWriter          Another implementation of IDataWriter that writes data points to
                            console standard output. It is meant to illustrate that the Outlier
                            program can be changed to use different data writer via configuration.


Usage Description
----------------------------------------------------------------------------------------------------

Debug Using Visual Studio

- Open the Outlier.sln in Visual Studio 2019.
- Build the solution.
- Press F5 to run Outlier.
- The app.config file is currently configured to
  * use CSVDataSource.dll to read data points in Outliers.csv located in the Outlier project folder.
  * use SimpleOutlierDetector.dll to identify outliers, which is configured to skip the first 10
    data points from outlier identification and identify a data point as outliers if its distance
    from the accumulated average is further than 3 sigma (standard deviation).
  * use CSVDataWriter.dll to output clean data points to clean.csv that will be created in the
    Outlier project folder.
  * use StdoutDataWriter.dll to output outlier data points identified to console standard output.

Run From Build Directory Executables

- DLL, EXE and configuration files are copied to Build folder under Outlier solution folder. The
  files are located in either Debug or Release subfolder, depending on which solution configuration
  is chosen in building the solution.
- The Outlier.exe.config file is currently configured to
  * use CSVDataSource.dll to read data points in Outliers.csv located in the same folder.
  * use SimplleOutlierDetector.dll to identifier outliers. Same configuration as when debugging
    from Visual Studio.
  * use CSVDataWriter.dll to output clean data points to clean.csv
  * Use CSVDataWriter.dll to output filtered outliers to filtered_outliers.csv

Changing Data Source to StdinDataSource.dll

- The Outlier.exe.config in Build folder has the StdinDataSource.dll configurations commented out.

        <add key="DataSourceDLL" value ="CSVDataSource.dll"/>
        <add key="DataSourceType" value ="CSVDataSource.CSVDataSource"/>
        <add key="CSVDataSourceFile" value ="Outliers.csv"/>
        <!-- <add key="DataSourceDLL" value ="StdinDataSource.dll"/> -->
        <!-- <add key="DataSourceType" value ="StdinDataSource.StdinDataSource"/> -->

- Simply change the above configuration section as below to change data source input from console
  standard input.

        <!-- <add key="DataSourceDLL" value ="CSVDataSource.dll"/>  -->
        <!-- <add key="DataSourceType" value ="CSVDataSource.CSVDataSource"/>  -->
        <add key="CSVDataSourceFile" value ="Outliers.csv"/>
        <add key="DataSourceDLL" value ="StdinDataSource.dll"/>
        <add key="DataSourceType" value ="StdinDataSource.StdinDataSource"/>

----------------------------------------------- END ------------------------------------------------