﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OutlierModel;

namespace StdinDataSource
{
    class StdinDataSource : IDataSource
    // a data source that takes data points from stdin. This is meant to illustrate the capability to switch data source easily.
    {
        public StdinDataSource()
        {
            Console.WriteLine("Input price in the following format. CTRL-Z to indicate end of input");
            Console.WriteLine("[dd/mm/yyyy], [price]");
        }

        public DataReadStatus GetNextDataPoint(out DataPoint dataPoint)
        {
            while (true)
            {
                string line = Console.ReadLine();
                if (line == null)
                {
                    dataPoint.date = DateTime.Now;
                    dataPoint.price = 0;
                    return DataReadStatus.Finish;
                }
                if (!CSVHelper.FromString(line, out dataPoint))
                {
                    Console.Error.WriteLine("Format error");
                }
                else
                {
                    return DataReadStatus.Success;
                }
            }
        }
        public void Dispose()
        {
        }
    }

}
