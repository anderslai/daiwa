﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OutlierModel;
using System.Configuration;
using System.IO;

namespace CSVDataWriter
{
    class CSVDataWriter : IDataWriter
    // a data writer that writes data points to CSV file
    {
        private CSVDataWriter()
        {

        }

        public CSVDataWriter(string dllName)
        {
            string cfgKey = dllName + "." + "CSVDataWriterFile";
            string filePath = ConfigurationManager.AppSettings.Get(cfgKey);
            csvDataWriter = new StreamWriter(filePath);
            csvDataWriter.WriteLine("Date,Price");
        }

        public bool WriteData(DataPoint dataPoint)
        {
            csvDataWriter.WriteLine(CSVHelper.ToString(dataPoint));
            return true;

        }

        public void Dispose()
        {
            csvDataWriter.Close();
        }

        private StreamWriter csvDataWriter;
    }
}
