﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Configuration;
using OutlierModel;
namespace Outlier
{
    class Program
    {
        static void Main(string[] args)
        {
            if (!LoadDataSource())
            {
                Console.Error.WriteLine("Failed to load data source.");
                return;
            }
            if (!LoadOutlierDetector())
            {
                Console.Error.WriteLine("Failed to load outlier detector.");
                return;
            }
            if (!LoadDataWriter())
            {
                Console.Error.WriteLine("Failed to load data writer.");
                return;
            }
            if (!LoadOutlierWriter())
            {
                Console.Error.WriteLine("Failed to load outlier writer.");
                return;
            }
            DataPointProcessor dpp = new DataPointProcessor(dataSource, outlierDetector, dataWriter, outlierWriter);
            dpp.Run();
        }

        private static bool LoadDataSource()
        {
            string dllPath = ConfigurationManager.AppSettings.Get("DataSourceDLL");
            if (dllPath == "")
            {
                return false;
            }
            Assembly dsAssembly = Assembly.LoadFrom(dllPath);
            string typeName = ConfigurationManager.AppSettings.Get("DataSourceType");
            Type type = dsAssembly.GetType(typeName);
            object instance = Activator.CreateInstance(type);
            dataSource = (IDataSource)instance;
            //GetNextDataPoint = null;
            //GetNextDataPoint = type.GetMethod("GetNextDataPoint", BindingFlags.Public | BindingFlags.Instance, null, new Type[] { typeof(DataPoint).MakeByRefType() }, null);
            return true;
        }

        private static bool LoadOutlierDetector()
        {
            string dllPath = ConfigurationManager.AppSettings.Get("OutlierDetectorDLL");
            if (dllPath == "")
            {
                return false;
            }
            Assembly dsAssembly = Assembly.LoadFrom(dllPath);
            string typeName = ConfigurationManager.AppSettings.Get("OutlierDetectorType");
            if (typeName == "")
            {
                return false;
            }
            Type type = dsAssembly.GetType(typeName);
            object instance = Activator.CreateInstance(type);
            outlierDetector = (IOutlierDetector)instance;
            //AddDataPoint = null;
            //AddDataPoint = type.GetMethod("AddDataPoint", BindingFlags.Public | BindingFlags.Instance, null, new Type[] { typeof(DataPoint) }, null);
            return true;
        }

        private static bool LoadDataWriter()
        {
            string dllPath = ConfigurationManager.AppSettings.Get("DataWriterDLL");
            if (dllPath == "")
            {
                return false;
            }
            Assembly dsAssembly = Assembly.LoadFrom(dllPath);
            string typeName = ConfigurationManager.AppSettings.Get("DataWriterType");
            if (typeName == "")
            {
                return false;
            }
            Type type = dsAssembly.GetType(typeName);
            object instance = Activator.CreateInstance(type, new object[] { "DataWriterDLL" });
            dataWriter = (IDataWriter)instance;
            //WriteData = null;
            //WriteData = type.GetMethod("WriteData", BindingFlags.Public | BindingFlags.Instance, null, new Type[] { typeof(DataPoint) }, null);
            return true;
        }

        private static bool LoadOutlierWriter()
        {
            string dllPath = ConfigurationManager.AppSettings.Get("OutlierWriterDLL");
            if (dllPath == "")
            {
                return false;
            }
            Assembly dsAssembly = Assembly.LoadFrom(dllPath);
            string typeName = ConfigurationManager.AppSettings.Get("OutlierWriterType");
            if (typeName == "")
            {
                return false;
            }
            Type type = dsAssembly.GetType(typeName);
            object instance = Activator.CreateInstance(type, new object[] { "OutlierWriterDLL" });
            outlierWriter = (IDataWriter)instance;
            return true;
        }

        private static IDataSource dataSource;
        private static IOutlierDetector outlierDetector;
        private static IDataWriter dataWriter;
        private static IDataWriter outlierWriter;

        //private static MethodInfo GetNextDataPoint;
        //private static MethodInfo AddDataPoint;
        //private static MethodInfo WriteData;
    }
}
