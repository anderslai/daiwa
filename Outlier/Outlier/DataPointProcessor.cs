﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OutlierModel;

namespace Outlier
{
    public class DataPointProcessor
    {
        public DataPointProcessor(IDataSource ds, IOutlierDetector od, IDataWriter dw, IDataWriter ow)
        {
            dataSource = ds;
            outlierDetector = od;
            dataWriter = dw;
            outlierWriter = ow;
        }

        public bool Run()
        {
            using (dataSource)
            using (dataWriter)
            {
                while (dataSource.GetNextDataPoint(out DataPoint dataPoint) != DataReadStatus.Finish)
                {
                    DataPointStatus s = outlierDetector.AddDataPoint(dataPoint);
                    if (s == DataPointStatus.Success)
                    {
                        if (!dataWriter.WriteData(dataPoint))
                        {
                            // error handling 
                            return false;
                        }
                    }
                    else if (s == DataPointStatus.Outlier)
                    {
                        if (!outlierWriter.WriteData(dataPoint))
                        {
                            // error handling 
                            return false;
                        }
                    }
                    else
                    {
                        // error handling
                        return false;
                    }
                }
            }

            return true;
        }

        private DataPointProcessor()    // disallow use of default constructor
        {
        }

        private IDataSource dataSource;             // data source from which data points are retrieved
        private IOutlierDetector outlierDetector;   // an interface to determine whether a data point is outlier or not
        private IDataWriter dataWriter;             // data writer to write clean data points (i.e. with outliers filtered out)
        private IDataWriter outlierWriter;          // data writer to write outlier data points
    }
}
