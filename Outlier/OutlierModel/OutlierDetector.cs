﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutlierModel
{
    public enum DataPointStatus
    {
        Success,
        Outlier,
        Failure
    }
    public interface IOutlierDetector
    // interface to accummulate data points and decide whether the data point to be added is considered outlier
    {
        DataPointStatus AddDataPoint(DataPoint dataPoint);
    }
}
