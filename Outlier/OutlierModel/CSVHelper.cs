﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutlierModel
{
    public class CSVHelper
    // Should place this helper class in a different helper project because it is not strictly related to model.
    // However, leave it here for the sake of simplicity
    {
        public static bool FromString(string line, out DataPoint dataPoint)
        {
            dataPoint.date = DateTime.Now;
            dataPoint.price = 0;
            int commaPos = line.IndexOf(',');
            if ((commaPos == -1) || (commaPos == line.Length - 1))
            {   // either comma not found or found at end of line
                return false;
            }
            string s1 = line.Substring(0, commaPos);
            string s2 = line.Substring(commaPos + 1);
            DateTime date;
            //if (!DateTime.TryParseExact(s1, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out date))
            if (!CSVHelper.FromString(s1, out date))
            {
                return false;
            }
            Decimal price;
            if (!Decimal.TryParse(s2, out price))
            {
                return false;
            }
            dataPoint.date = date;
            dataPoint.price = price;
            return true;
        }

        public static bool FromString(string dateString, out DateTime dateTime)
        // this function is introduced to workaround the problem of TryParse() dealing with dd/mm/yyyy
        {
            dateTime = DateTime.Now;
            if (dateString.Length != 10)
            {
                return false;
            }
            if ((dateString[2] != '/') || (dateString[5] != '/'))
            {
                return false;
            }
            string sDD = dateString.Substring(0, 2);
            string sMM = dateString.Substring(3, 2);
            string sYYYY = dateString.Substring(6, 4);
            int dd = 0;
            int mm = 0;
            int yyyy = 0;
            bool b = int.TryParse(sDD, out dd) && int.TryParse(sMM, out mm) && int.TryParse(sYYYY, out yyyy);
            if (!b)
            {
                return false;
            }

            try
            {
                dateTime = new DateTime(yyyy, mm, dd);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public static string ToString(DataPoint dp)
        {
            //StringBuilder sb = new StringBuilder("", 100);
            //sb.AppendFormat("{0},{1}", dp.date.ToString("dd/MM/yyyy"), dp.price.ToString());
            //return sb.ToString();
            return $"{dp.date.ToString("dd/MM/yyyy")},{dp.price.ToString()}";
        }

    }
}
