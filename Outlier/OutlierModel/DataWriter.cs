﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutlierModel
{
    public interface IDataWriter : IDisposable
    // interface for writing clean version of data points or writing outlier data points
    {
        bool WriteData(DataPoint dataPoint);
    }
}
