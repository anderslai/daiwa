﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutlierModel
{
    public struct DataPoint
    {
        public DateTime date;
        public decimal price;
    }

    public enum DataReadStatus
    {
        Success,
        Finish,
        Failure,
    }
    public interface IDataSource : IDisposable
    // interface to retrieve data point
    {
        DataReadStatus GetNextDataPoint(out DataPoint dataPoint);
    }
}
