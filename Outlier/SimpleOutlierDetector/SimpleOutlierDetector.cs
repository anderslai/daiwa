﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OutlierModel;
using System.Configuration;


namespace SimpleOutlierDetector
{
    class SimpleOutlierDetector : IOutlierDetector
    // A very simple outlier detector implementation.
    // If distance of incoming data point from average of accumulated data points > 3 times of standard deviation,
    // then the data point is considered outlier.
    // The first 10 data points are excluded from outlier detection
    {
        public SimpleOutlierDetector()
        {
            string numDP = ConfigurationManager.AppSettings.Get("SimpleOutlierDetector.MinDataPointsForAvg");
            string numSigma = ConfigurationManager.AppSettings.Get("SimpleOutlierDetector.NumSigma");
            dataPoints = new List<DataPoint>();
            minNumDataPoints = int.Parse(numDP);
            sigmaMultiple = int.Parse(numSigma);
        }

        public SimpleOutlierDetector(int minDataPointsForAvg, int numSigma)
        {
            dataPoints = new List<DataPoint>();
            minNumDataPoints = minDataPointsForAvg;
            sigmaMultiple = numSigma;
        }
        public DataPointStatus AddDataPoint(DataPoint dataPoint)
        {
            // use of double type assumed to be good enough for outlier detection
            double newAverage = (average * dataPoints.Count + (double)dataPoint.price) / (dataPoints.Count + 1);
            if (dataPoints.Count > minNumDataPoints)
            {
                double sigma = 0;
                foreach (var dp in dataPoints)
                {
                    sigma += Math.Pow((double)dp.price - newAverage, 2);
                }
                sigma = sigma / (dataPoints.Count + 1);
                sigma = Math.Sqrt(sigma);

                double distance = Math.Abs((double)dataPoint.price - newAverage);
                if (distance > sigma * sigmaMultiple)
                {
                    return DataPointStatus.Outlier;
                }
            }
            average = newAverage;
            dataPoints.Add(dataPoint);
            return DataPointStatus.Success;
        }

        private double average;
        private List<DataPoint> dataPoints;
        private readonly int minNumDataPoints;// = 10;
        private readonly int sigmaMultiple;// = 3;
    }
}
