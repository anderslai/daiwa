﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Globalization;
using OutlierModel;
using System.Configuration;

namespace CSVDataSource
{
    public class CSVDataSource : IDataSource
    // a data source that retrieve data points from CSV file
    {
        public CSVDataSource()
        {
            string filePath = ConfigurationManager.AppSettings.Get("CSVDataSourceFile");
            csvReader = new StreamReader(filePath);
        }

        public CSVDataSource(string filePath)
        {
            csvReader = new StreamReader(filePath);
        }

        public DataReadStatus GetNextDataPoint(out DataPoint dataPoint)
        {
            string line;
            while ((line = csvReader.ReadLine()) != null)
            {
                if (!CSVHelper.FromString(line, out dataPoint))
                {
                    continue;
                }
                return DataReadStatus.Success;
            }
            dataPoint.date = DateTime.Now;
            dataPoint.price = 0;
            return DataReadStatus.Finish;
        }

        public void Dispose()
        {
            csvReader.Close();
        }

        private StreamReader csvReader;
    }
}
