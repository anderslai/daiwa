﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OutlierModel;

namespace StdoutDataWriter
{
    class StdoutDataWriter : IDataWriter
    // A data writer that writes data points to stdout. This is to illustrate it is possible to switch data writer implementation easily
    {
        private StdoutDataWriter()
        {
        }

        public StdoutDataWriter(string dllName)
        {
            Console.WriteLine("Date,Price");
        }

        public bool WriteData(DataPoint dataPoint)
        {
            Console.WriteLine(CSVHelper.ToString(dataPoint));
            return true;
        }

        public void Dispose()
        {
        }
    }
}
